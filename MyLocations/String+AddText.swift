//
//  String+AddText.swift
//  MyLocations
//
//  Created by Rodney Coleman Jr. on 4/3/16.
//  Copyright © 2016 rc6886. All rights reserved.
//

extension String {
    mutating func addText(text: String?, withSeparator separator: String = "") {
        if let text = text {
            if !isEmpty {
                self += separator
            }
            self += text
        }
    }
}