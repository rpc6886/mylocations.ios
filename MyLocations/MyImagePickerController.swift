//
//  MyImagePickerController.swift
//  MyLocations
//
//  Created by Rodney Coleman Jr. on 4/3/16.
//  Copyright © 2016 rc6886. All rights reserved.
//

import UIKit
class MyImagePickerController: UIImagePickerController {
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
}
